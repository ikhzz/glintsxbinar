const EventEmitter = require('events'); // Import
const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

 // Initialize an instance because it is a class
const my = new EventEmitter();

// Registering a listener
my.on("Login Failed", function(email) {
  // TODO: Saving the login trial count in the database
  console.log(email, "is failed to login!");
})

my.on("Login Success", function(email) {
  // TODO: Saving the login trial count in the database
  console.log(email, "is login success!");
  require('../../Week-2/Day-10/assigment2') // task day 11 add this line
  rl.close()
})

function login(email, password) {
  const passwordStoredInDatabase = "123456";

  if (password !== passwordStoredInDatabase) {
    my.emit("Login Failed", email); // Pass the email to the listener
  } else {
    // Do something
    my.emit("Login Success", email); 
  }
}

rl.question("Email: ", function(email) {
  rl.question("Password: ", function(password) {
    login(email, password) // Run login function
  })
})
