// Two Dimention Class
const Square = require("./2d/square.js");
const Rectangle = require("./2d/rectangle.js");
const Triangle = require("./2d/triangle.js");

// Three Dimention Class
const Cube = require('./3d/Cube.js')
const Cone = require('./3d/Cone.js')
const Beam = require('./3d/Beam.js')
const Tube = require('./3d/Tube.js')

module.exports = { Square, Rectangle, Triangle, Cube, Cone, Beam, Tube};
