const ThreeDimention = require('../type/ThreeDimension.js');

class Cone extends ThreeDimention {

  constructor(radius, height){
    super('Cone')
    this.radius = radius
    this.height = height
  }

  introduce = () => {
    super.introduce()
    console.log(`This is ${this.name}`)
  }
  calculateVolume = () => {
    super.calculateVolume()
    const volume = (0.3 * Math.PI * this.radius * this.radius * this.height).toFixed(2)
    console.log(`${this.name} Volume for height: ${this.height}m and radius: ${this.radius}m is ${volume}m3 \n`)
  }
}

module.exports = Cone