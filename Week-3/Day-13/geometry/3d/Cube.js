const ThreeDimention = require('../type/ThreeDimension.js');

class Cube extends ThreeDimention {

  constructor(side){
    super('Cube')
    this.side = side
  }

  introduce = () => {
    super.introduce()
    console.log(`This is ${this.name}`)
  }
  calculateVolume = () => {
    super.calculateVolume()
    console.log(`${this.name} Volume for side: ${this.side}m is ${this.side * this.side * this.side}m3 \n`)
  }
}

module.exports = Cube