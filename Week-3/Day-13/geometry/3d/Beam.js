const ThreeDimention = require('../type/ThreeDimension.js');

class Beam extends ThreeDimention {

  constructor(length, width, height){
    super('Beam')
    this.length = length
    this.width = width
    this.height = height
  }

  introduce = () => {
    super.introduce()
    console.log(`This is ${this.name}`)
  }
  calculateVolume = () => {
    super.calculateVolume()
    const volume = this.length * this.width * this.height
    console.log(`${this.name} Volume for height: ${this.height}m, width: ${this.width}m, length: ${this.length} is ${volume}m3 \n`)
  }
}

module.exports = Beam