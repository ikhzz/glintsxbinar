const ThreeDimention = require('../type/ThreeDimension.js');

class Tube extends ThreeDimention {

  constructor(radius, height){
    super('Tube')
    this.radius = radius
    this.height = height
  }

  introduce = () => {
    super.introduce()
    console.log(`This is ${this.name}`)
  }
  
  calculateVolume = () => {
    super.calculateVolume()
    const volume = (Math.PI * this.radius * this.radius * this.height).toFixed(2)
    console.log(`${this.name} Volume for height: ${this.height}m and radius: ${this.radius}m is ${volume}m3 \n`)
  }
}

module.exports = Tube