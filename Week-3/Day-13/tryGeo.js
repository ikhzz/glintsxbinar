const { Square, Rectangle, Triangle, Cube, Cone, Beam, Tube } = require("./geometry");

let squareOne = new Square(12);
squareOne.calculateArea();

let rectangleOne = new Rectangle(11, 12);
rectangleOne.calculateArea();
rectangleOne.calculateCircumference();

let cube = new Cube(2)
cube.calculateVolume()

let cone = new Cone(3, 2)
cone.calculateVolume()

let beam = new Beam(2,3 ,4)
beam.calculateVolume()

let tube = new Tube(2, 3)
tube.calculateVolume()

