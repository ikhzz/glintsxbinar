const express = require("express"); // Import express
const app = express(); // Make express app
const route = require("./routes/routes"); // Import helloRoute
const port = process.env.port || 3000

/* If user go to http://localhost:3000
It will go to here*/
app.use("/", route);

// This is port for this server
app.listen(port, () => console.log("Server running on 3000!"));