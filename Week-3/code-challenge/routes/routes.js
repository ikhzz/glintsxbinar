const express = require("express"); // Import express
const router = express.Router(); // Make a router
const MainController = require("../controllers/MainController"); // Import HelloController

// -- Home Route
// If user go to http://localhost:3000 (GET)
router.get("/:fikri", MainController.get);

// If user go to http://localhost:3000 (POST)
router.post("/:fikri", MainController.post);

// If user go to http://localhost:3000 (PUT)
router.put("/:fikri", MainController.put);

// If user go to http://localhost:3000 (DELETE)
router.delete("/:fikri", MainController.delete);

// -- Username Route ?
router.get("/fikri", MainController.get);
router.post("/fikri", MainController.get);
router.put("/fikri", MainController.get);
router.delete("/fikri", MainController.get);

module.exports = router; // Export router