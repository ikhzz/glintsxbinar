const msg = `My Full Name is Mohammad Zul Fikri Dwi Ananta`;

class MainController {

  get(req, res) {
    console.log("This is example GET!");
    console.log(msg)
    res.send({ name: 'Fikri', msg: msg, type: 'GET', yourParam: req.params });
  }

  // If user uses POST method
  post(req, res) {
    console.log("This is example POST!");
    console.log(msg)
    res.send({ name: 'Fikri', msg: msg, type: 'POST', yourParam: req.params });
  }

  // If user uses PUT method
  put(req, res) {
    console.log("This is example PUT!");
    console.log(msg)
    res.send({ name: 'Fikri', msg: msg, type: 'PUT', yourParam: req.params });
  }

  // If user uses DELETE method
  delete(req, res) {
    console.log("This is example DELETE!");
    console.log(msg)
    res.send({ name: 'Fikri', msg: msg, type: 'DELETE', yourParam: req.params });
  }
}

module.exports = new MainController();