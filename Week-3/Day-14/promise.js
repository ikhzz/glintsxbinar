const fs = require('fs');
const fileName = ['./text/lineOne.txt', './text/lineTwo.txt', './text/lineThree.txt',
  './text/lineFour.txt', './text/lineFive.txt', './text/lineSix.txt', './text/lineSeven.txt',
  './text/lineEight.txt', './text/lineNine.txt', './text/lineTen.txt']
const nodes = `You can never understand everything. But, you should push yourself to understand the system.\n- Ryan Dahl (Creator of Node JS)\n`;

const promiseReturn = (file) => {
  return new Promise((resolve, reject) => {
    fs.readFile(file, 'utf-8', (err, res) => {
      if(err) reject(err)
      resolve(res)
    })
  })
}

const helper = (content, file) => {
  console.log(content)
  return promiseReturn(file)
}

const promiseTask = (arr, nodes) => {
  promiseReturn(arr[0])
    .then(r => helper(r, arr[1]))
    .then(r => helper(r, arr[2]))
    .then(r => helper(r, arr[3]))
    .then(r => helper(r, arr[4]))
    .then(r => helper(r, arr[5]))
    .then(r => helper(r, arr[6]))
    .then(r => helper(r, arr[7]))
    .then(r => helper(r, arr[8]))
    .then(r => helper(r, arr[9]))
    .then(r => console.log(r))
    .catch(err => console.log(err))
    .finally(()=> console.log(nodes))
}

promiseTask(fileName, nodes)

module.exports = {promiseReturn, fileName, nodes}