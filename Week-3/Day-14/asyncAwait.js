const {promiseReturn, fileName, nodes} = require('./promise.js')

const asyncTask = async (arr, nodes) => {
  arr = arr.map(i => promiseReturn(i))
  try {
    const data = await Promise.all(arr)
    for(i of data) console.log(i)
  } catch (error) {
   console.log(error) 
  } finally {
    console.log(nodes)
  }
}

asyncTask(fileName, nodes)