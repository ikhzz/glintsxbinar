const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import controller
const PemasokController = require("../controllers/pemasokController.js");

const PemasokValidator = require("../middlewares/validators/pemasokValidator.js");

router.get("/", PemasokController.getAll); // If GET (/transaksi), will go to getAll function in transaksiController class
router.get("/:id", PemasokValidator.getOne, PemasokController.getOne);
router.post("/", PemasokValidator.createPemasok, PemasokController.createPemasok);
router.put("/:id", PemasokValidator.updatePemasok, PemasokController.updatePemasok);
router.delete("/:id", PemasokValidator.getOne, PemasokController.deletePemasok);

module.exports = router;
