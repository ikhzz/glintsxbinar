const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import controller
const PelangganController = require("../controllers/pelangganController.js");

const PelangganValidator = require("../middlewares/validators/pelangganValidator");

router.get("/", PelangganController.getAll); // If GET (/transaksi), will go to getAll function in transaksiController class
router.get("/:id", PelangganValidator.getOne, PelangganController.getOne);
router.post("/", PelangganValidator.createPelanggan, PelangganController.createPelanggan);
router.put("/:id", PelangganValidator.updatePelanggan, PelangganController.updatePelanggan);
router.delete("/:id", PelangganValidator.getOne, PelangganController.deletePelanggan);

module.exports = router;