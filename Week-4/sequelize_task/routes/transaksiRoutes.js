const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import controller
const transaksiController = require("../controllers/transaksiController.js");

const transaksiValidator = require("../middlewares/validators/transaksiValidator.js")

router.get("/", transaksiController.getAll); // If GET (/transaksi), will go to getAll function in transaksiController class
router.get("/:id", transaksiValidator.getOne, transaksiController.getOne)

router.post('/', transaksiValidator.create, transaksiController.createTransaksi)
router.put('/:id', transaksiValidator.update, transaksiController.updateTransaksi)
router.delete('/:id', transaksiValidator.getOne, transaksiController.deleteTransaksi)

module.exports = router; // Export router