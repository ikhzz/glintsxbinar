const express = require("express"); // Import express
const router = express.Router(); // Make a router

const { imageUpload } = require("../middlewares/uploads/imageUpload.js");
const BarangValidator = require("../middlewares/validators/barangValidator");
// Import controller
const BarangController = require("../controllers/barangController.js");

router.get("/", BarangController.getAll); // If GET (/transaksi), will go to getAll function in transaksiController class
router.get("/:id", BarangValidator.getOne, BarangController.getOne);
router.post("/", imageUpload, BarangValidator.create, BarangController.createBarang);
router.put("/:id", BarangValidator.updateBarang, BarangController.updateBarang);
router.delete("/:id", BarangValidator.getOne, BarangController.deleteBarang);

module.exports = router;