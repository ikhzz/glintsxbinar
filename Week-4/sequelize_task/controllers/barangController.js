const { barang, pemasok } = require("../models"); // Import all models

// controller class for request to barang table
class BarangController {
  // controller method for all item barang request
  async getAll(req, res) {
    try {
      // find all data of barang table
      const data = await barang.findAll({
        attributes: ["id", "nama", "harga", ["createdAt", "waktu"]], // just these attributes that showed
        include: [
          // Include is join
          {
            model: pemasok,
            attributes: ["nama"], // just this attribute from pemasok that showed
          },
        ],
      });
      // send response of all data
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Controller",
        error: e,
      });
    }
  }
  // controller method for single item request
  async getOne(req, res){
    try{
      // get the requested item data
      const data = await barang.findOne({
        where: {id: req.params.id},
        attributes: ["id", "nama", "harga", ["createdAt", "waktu"]],
        include: [
          // Include is join
          {
            model: pemasok,
            attributes: ["nama"], // just this attribute from Barang that showed
          },
        ],
      })
      // send the requested data
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch(e) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Controller",
        error: e,
      });
    }
  }
  // controller method for create item request
  async createBarang(req, res){
    try {
      // create the new item for barang table
      const createdData = await barang.create({
        nama: req.body.nama_barang,
        harga: req.body.harga_barang,
        id_pemasok: req.body.id_pemasok,
        image: req.body.image
      })
      // send the detail of created data
      return res.status(200).json({
        message: "Success",
        data: createdData,
      });
    } catch (e) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Controller",
        error: e,
      });
    }
  }
  // controller method for update item request
  async updateBarang(req, res){
    try {
      // sequelize update method doesn't return detail value of the updated data
      await barang.update({
        nama: req.body.nama_barang,
        harga: req.body.harga_barang,
      },{where: {id: req.params.id}})
      // get the updated item data for response
      const updated = await barang.findOne({
        where: {id: req.params.id},
        attributes: ["id", "nama", "harga", ["createdAt", "waktu"]],
        include: [
          // Include is join
          {
            model: pemasok,
            attributes: ["nama"], // just this attribute from Barang that showed
          },
        ],
      })
      // send the updated data
      return res.status(200).json({
        message: "Success",
        data: updated,
      });
    } catch (e) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Controller",
        error: e,
      });
    }
  }
  // controller method for delete item request
  async deleteBarang(req, res){
    try {
      /* -you can either get the item requested to delete first or just send all data
      -sequelize destroy/delete method doesn't return detail value of the destroyed/deleted data */
      await barang.destroy({
        where: {id: req.params.id}
      })
      // get all data as response detail
      const data = await barang.findAll({
        // find all data of barang table
        attributes: ["id", "nama", "harga", ["createdAt", "waktu"]], // just these attributes that showed
        include: [
          // Include is join
          {
            model: pemasok,
            attributes: ["nama"], // just this attribute from pemasok that showed
          },
        ],
      });
      // send all data
      return res.status(200).json({
        message: "Success",
        data
      });
    } catch (e) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Controller",
        error: e,
      });
    }
  }
}

module.exports = new BarangController()