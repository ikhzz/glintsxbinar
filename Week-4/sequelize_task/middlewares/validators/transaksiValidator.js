const { transaksi, barang, pelanggan } = require("../../models");
const validator = require("validator");

module.exports.create = async(req, res, next) => {
  try {
    // Find barang and pelanggan
    let findData = await Promise.all([
      barang.findOne({
        where: { id: req.body.id_barang },
      }),
      pelanggan.findOne({
        where: { id: req.body.id_pelanggan },
      }),
    ]);

    // Create errors variable
    let errors = [];

    // If barang not found
    if (!findData[0]) {
      errors.push("Barang Not Found");
    }

    // If pelanggan not found
    if (!findData[1]) {
      errors.push("Pelanggan Not Found");
    }
    
    if (!validator.isNumeric(req.body.jumlah)) {
      errors.push("Jumlah must be a number");
    }

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // It means that will be go to the next middleware
    next();
  } catch (e) {
    return res.status(501).json({
      message: "Internal Server Error At Validator",
      error: e,
    });
  }
}

module.exports.update = async (req, res, next) => {
  try {
    // Find barang, pelanggan, transaksi
    let findData = await Promise.all([
      barang.findOne({
        where: { id: req.body.id_barang },
      }),
      pelanggan.findOne({
        where: { id: req.body.id_pelanggan },
      }),
      transaksi.findOne({
        where: { id: req.params.id }, // because we will update transaksi
      }),
    ]);

    // Create errors variable
    let errors = [];

    // If barang not found
    if (!findData[0]) {
      errors.push("Barang Not Found");
    }
    // If pelanggan not found
    if (!findData[1]) {
      errors.push("Pelanggan Not Found");
    }

    // If transaksi not found
    if (!findData[2]) {
      errors.push("Transaksi Not Found");
    }

    if (!validator.isNumeric(req.body.jumlah)) {
      errors.push("Jumlah must be a number");
    }

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // It means that will be go to the next middleware
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error At Validator",
      error: e,
    });
  }
};

module.exports.getOne = async(req, res, next) => {
  try {
    const errors = []
    
    if (!validator.isNumeric(req.params.id)) {
      errors.push("Jumlah must be a number");
    }
    const getData = await transaksi.findOne({
        where: {id: req.params.id},
    })
  
    if(transResult == null){
      errors.push("Transaksi Not Found");
    }
    
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    next()  
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error At Validator",
      error: e,
    });
  } 
}