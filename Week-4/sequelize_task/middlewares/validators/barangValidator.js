const { barang, pemasok } = require("../../models"); // Import all models
const validator = require("validator");

// validator class for request to barang table
class BarangValidator {
  // validator method for single item request
  async getOne(req, res, next) {
    try {
      const errors = [];
      // validator if the request id is not a number
      if (!validator.isNumeric(req.params.id)) {
        errors.push("Request id must be a number");
      }
      // get the requested data
      const getData = await barang.findOne({
        where: { id: req.params.id },
      });
      // validator if the requested data doesn't exit
      if (getData == null) {
        errors.push("Barang Not Found");
      }
      // trigger if the request has error
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      // continue to next controller
      next();
    } catch (e) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Validator",
        error: e,
      });
    }
  }
  // validator method for update item request
  async updateBarang(req, res, next) {
    try {
      const errors = [];
      // validator if the request id is not a number
      if (!validator.isNumeric(req.params.id)) {
        errors.push("Request id must be a number");
      }
      // validator for item name value
      if (!validator.isAlpha(req.body.nama_barang, ['en-US'], {ignore: ' '})) {
        errors.push("Nama Barang Bukanlah Angka");
      }
      // validator for item price value
      if (!validator.isNumeric(req.body.harga_barang)) {
        errors.push("Harga must be a number");
      }
      // get the requested data
      const getData = await barang.findOne({
        where: { id: req.params.id },
      });
      // validator if the requested data doesn't exit
      if (getData == null) {
        errors.push("Barang Not Found");
      }
      // trigger if the request has error
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
          getData
        });
      }
      // continue to next controller
      next();
    } catch (e) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Validator",
        error: e,
      });
    }
  }
  // validator method for create item request
  async create(req, res, next) {
    try {
      // Find Pemasok
      let findPemasok = await pemasok.findOne({
        where: {
          id: req.body.id_pemasok,
        },
      });

      let errors = [];
      console.log(req.body)
      // Pemasok not found
      if (findPemasok == null) {
        errors.push("Pemasok Not Found");
      }

      //Check harga is number
      if (!validator.isNumeric(req.body.harga_barang)) {
        errors.push("Harga must be a number");
      }

      // If errors length > 0, it will make errors message
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }

      // It means that will be go to the next middleware
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error At Validator",
        error: e,
      });
    }
  }
}

module.exports = new BarangValidator();
