const { pelanggan } = require("../../models"); // Import all models
const validator = require("validator");

// validator class for request to pelanggan table
class PelangganValidator{
  // validator method for single item request
  async getOne(req, res, next){
    try {
      const errors = []
      // validator if the request id is not a number
      if (!validator.isNumeric(req.params.id)) {
        errors.push("Idnya harus angka");
      }
      // get the requested data
      const getData = await pelanggan.findOne({
        where: {id: req.params.id}
      })
      // validator if the requested data doesn't exit
      if(getData == null){
        errors.push("Pemasok Not Found");
      }
      // trigger if the request has error
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      // continue to next controller
      next()
    } catch (e) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Validator",
        error: e,
      });
    }
  }
  // validator method for create item request
  async createPelanggan(req, res, next){
    try {
      const errors = []
      // validator if the request name is not an alphabet
      if (!validator.isAlpha(req.body.nama_pelanggan, ['en-US'], {ignore: ' '})) {
        errors.push("Nama Pemasok Bukanlah Angka");
      }
      // trigger if the request has error
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      // continue to next controller
      next()
    } catch (e) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Validator",
        error: e,
      });
    }
  }
  // validator method for update item request
  async updatePelanggan(req, res, next){
    try {
      const errors = []
      // validator if the request id is not a number
      if (!validator.isNumeric(req.params.id)) {
        errors.push("Idnya harus angka");
      }
      // validator if the request name is not an alphabet
      if (!validator.isAlpha(req.body.nama_pelanggan, ['en-US'],{ignore: ' '})) {
        errors.push("Nama Pelanggan Bukanlah Angka");
      }
      // get the requested data
      const getData = await pelanggan.findOne({
        where: {id: req.params.id}
      })
      // validator if the requested data doesn't exit
      if(getData == null) {
        errors.push("Pemasok Not Found");
      }
      // trigger if the request has error
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      // continue to next controller
      next()
    } catch (error) {
      // catch if async code failed
      return res.status(500).json({
        message: "Internal Server Error At Validator",
        error: e,
      });
    }
  }
}

module.exports = new PelangganValidator()