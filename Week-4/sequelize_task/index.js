// Express
const express = require("express");
const app = express();

// Import routes
const transaksiRoutes = require("./routes/transaksiRoutes.js");
const barangRoutes = require("./routes/barangRoutes.js");
const pemasokRoutes = require("./routes/pemasokRoutes.js");
const pelangganRoutes = require("./routes/pelangganRoutes.js");


//Set body parser for HTTP post operation
app.use(express.json()); // support json encoded bodies
app.use(
  express.urlencoded({
    extended: false,
  })
); // support encoded bodies
// set static assets to public directory (usually for images, videos, and other files)


// app.use(bodyParser());
app.use(express.static("public"));

app.use("/transaksi", transaksiRoutes); // if accessing localhost:3000/transaksi/* we will go to transaksiRoutes
app.use("/barang", barangRoutes);
app.use("/pemasok", pemasokRoutes);
app.use("/pelanggan", pelangganRoutes);

// Server running
app.listen(3000, () => {
  console.log("server running on port 3000")
});