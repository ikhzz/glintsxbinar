require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
})

const express = require('express');
const app = express();

const transaksiRoutes = require('./routes/transaksiRoutes')
const barangRoutes = require('./routes/barangRoutes')
const pemasokRoutes = require('./routes/pemasokRoutes')
const pelangganRoutes = require('./routes/pelangganRoutes')
//Set body parser for HTTP post operation
app.use(express.json()); // support json encoded bodies
app.use(express.urlencoded({
  extended: false
})); // support encoded bodies

app.use('/transaksi', transaksiRoutes);
app.use('/barang', barangRoutes);
app.use('/pemasok', pemasokRoutes);
app.use('/pelanggan', pelangganRoutes);

app.listen(3000 ,()=> {
  console.log("Server Running On Port 3000")
}); // make port 3000 for this app