const validator = require("validator");
const { ObjectId } = require("mongodb");
const MongoInit = require("../../models");

// validator barang class extend connection from monggoinit class
class BarangValidator extends MongoInit {
  constructor() {
    super();
  }
  // validator method for single item request
  getOne = async (req, res, next) => {
    try {
      // check the requested data
      const findData = await this._barang.findOne({
        _id: ObjectId(req.params.id),
      });
      // if the data doesn't exist
      if (findData == null) {
        return res.status(400).json({
          message: "Data Not Found",
        });
      }
      // next router
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // validator method for create item request
  createBarang = async (req, res, next) => {
    const errors = [];
    try {
      // check the additional data for create item request
      const findData = await this._pemasok.findOne({
        _id: ObjectId(req.body.id_pemasok),
      });

      // If pemasok not found
      if (!findData == null) {
        errors.push("Pemasok Not Found");
      }

      if (!validator.isNumeric(req.body.harga_barang)) {
        errors.push("Jumlah must be a number");
      }
      if (!validator.isAlpha(req.body.nama_barang, ["en-US"], this._ignore)) {
        errors.push("Jumlah must be a number");
      }

      // If errors length > 0, it will make errors message
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }

      // add some req.body for used in Controller
      req.body.pemasok = findData;

      // It means that will be go to the next middleware
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // validator method for update item request
  updateBarang = async (req, res, next) => {
    const errors = [];
    try {
      // check the additional data for update item request
      const findData = await Promise.all([
        this._barang.findOne({
          _id: ObjectId(req.params.id),
        }),
        this._pemasok.findOne({
          _id: ObjectId(req.body.id_pemasok),
        }),
      ]);

      if (findData[0] == null) {
        errors.push("Barang Not Found");
      }

      if (findData[1] == null) {
        errors.push("Pemasok Not Found");
      }

      if (!validator.isNumeric(req.body.harga_barang)) {
        errors.push("Jumlah must be a number");
      }

      if (!validator.isAlpha(req.body.nama_barang, ["en-US"], this._ignore)) {
        errors.push("Jumlah must be a number");
      }
      // set the additional data to request body
      req.body.pemasok = findData[1];
      // If errors length > 0, it will make errors message
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      // next router
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
}

module.exports = new BarangValidator();
