const validator = require("validator");
const { ObjectId } = require("mongodb");
const MongoInit = require("../../models");

// validator transaksi class extend connection from monggoinit class
class TransaksiValidator extends MongoInit {
  constructor() {
    // call intial connection
    super();
  }
  // validator method for single item request
  getOne = async (req, res, next) => {
    try {
      // check the requested data
      const findData = await this._transaksi.findOne({
        _id: new ObjectId(req.params.id),
      });
      // if the data doesn't exist
      if (findData == null) {
        return res.status(400).json({
          message: "Data Not Found",
        });
      }
      // next router
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error at validator",
        error: e,
      });
    }
  };
  // validator method for create item request
  createTransaksi = async (req, res, next) => {
    const errors = [];
    try {
      // check the additional data for create item request
      let findData = await Promise.all([
        this._barang.findOne({ _id: ObjectId(req.body.id_barang) }),
        this._pelanggan.findOne({
          _id: ObjectId(req.body.id_pelanggan),
        }),
      ]);

      let errors = [];

      // If barang not found
      if (!findData[0]) {
        errors.push("Barang Not Found");
      }

      // If pelanggan not found
      if (!findData[1]) {
        errors.push("Pelanggan Not Found");
      }
      // if req is not a number
      if (!validator.isNumeric(req.body.jumlah)) {
        errors.push("Jumlah must be a number");
      }

      // If errors length > 0, it will make errors message
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }

      // add some req.body for used in Controller
      req.body.barang = findData[0];
      req.body.pelanggan = findData[1];
      req.body.total = eval(findData[0].harga.toString()) * req.body.jumlah; // Calculate total of transaksi

      // It means that will be go to the next middleware
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // validator method for update item request
  updateTransaksi = async (req, res, next) => {
    const errors = [];
    try {
      // Get barang and pelanggan
      let findData = await Promise.all([
        this._barang.findOne({
          _id: new ObjectId(req.body.id_barang),
        }),
        this._pelanggan.findOne({
          _id: new ObjectId(req.body.id_pelanggan),
        }),
        this._transaksi.findOne({
          _id: new ObjectId(req.params.id),
        }),
      ]);
      // Create errors variable
      let errors = [];

      // If barang not found
      if (!findData[0]) {
        errors.push("Barang Not Found");
      }

      // If pelanggan not found
      if (!findData[1]) {
        errors.push("Pelanggan Not Found");
      }

      if (!findData[2]) {
        errors.push("Transaksi Not Found");
      }

      if (!validator.isNumeric(req.body.jumlah)) {
        errors.push("Jumlah must be a number");
      }

      // If errors length > 0, it will make errors message
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }

      // add some req.body for used in Controller
      req.body.barang = findData[0];
      req.body.pelanggan = findData[1];
      req.body.total = eval(findData[0].harga.toString()) * req.body.jumlah; // Calculate total of transaksi

      // It means that will be go to the next middleware

      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
}

module.exports = new TransaksiValidator();
