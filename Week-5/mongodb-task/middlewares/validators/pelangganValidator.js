const validator = require("validator");
const { ObjectId } = require("mongodb");
const MongoInit = require("../../models");

// validator pemasok class extend connection from monggoinit class
class PelangganValidator extends MongoInit {
  constructor() {
    super();
  }
  // validator method for single item request
  getOne = async (req, res, next) => {
    const errors = [];
    try {
      // check the requested data
      const findData = await this._pelanggan.findOne({
        _id: ObjectId(req.params.id),
      });
      // if the requested data doesn't exist
      if (findData == null) {
        return res.status(400).json({
          message: "Request Not Found",
        });
      }
      // next router
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error at validator",
        error: e,
      });
    }
  };
  // validator method for create item request
  createPelanggan = (req, res, next) => {
    if (req.body.nama_pelanggan) {
      if (!validator.isAlpha(req.body.nama_pelanggan, ["en-US"], this._ignore)) {
        res.status(400).json({
          message: "Name is not valid",
        });
      }
      // next route
      next();
    } else {
      return res.status(500).json({
        message: "Bad Request",
      });
    }
  };
  // validator method for update item request
  updatePelanggan = async (req, res, next) => {
    const errors = [];
    try {
      // check the additional data for update item request
      const findData = await this._pelanggan.findOne({
        _id: ObjectId(req.params.id),
      });
      if (findData == null) errors.push("Pelanggan Not Found");

      if (!validator.isAlpha(req.body.nama_pelanggan, ["en-US"], this._ignore)) {
        errors.push("Name is Not Valid");
      }

      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      // next route
      next();
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error at validator",
      });
    }
  };
}

module.exports = new PelangganValidator();
