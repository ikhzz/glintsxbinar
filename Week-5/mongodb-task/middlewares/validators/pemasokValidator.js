const validator = require("validator");
const { ObjectId } = require("mongodb");
const MongoInit = require("../../models");

// validator pemasok class extend connection from monggoinit class
class PemasokValidator extends MongoInit {
  constructor() {
    super();
  }
  // validator method for single item request
  getOne = async (req, res, next) => {
    try {
      // check the requested data
      const checkData = await this._pemasok.findOne({
        _id: ObjectId(req.params.id),
      });
      // if the requested data doesn't exist
      if (checkData == null) {
        return res.status(400).json({
          message: "Data Not Found",
        });
      }
      // next router
      next();
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error At Validator",
        error: e,
      });
    }
  };
  // validator method for create item request
  createPemasok = async (req, res, next) => {
    if (req.body.nama_pemasok != " ") {
      if (!validator.isAlpha(req.body.nama_pemasok, ["en-US"], this._ignore)) {
        res.status(400).json({
          message: "Name is not valid",
        });
      }
      // next route
      next();
    } else {
      return res.status(500).json({
        message: "Bad Request",
      });
    }
  };
  // validator method for update item request
  updatePemasok = async (req, res, next) => {
    const errors = [];
    try {
      // check the additional data for update item request
      const findData = await this._pemasok.findOne({
        _id: ObjectId(req.params.id),
      });

      if (findData == null) errors.push("Pemasok Not Found");

      if (!validator.isAlpha(req.body.nama_pemasok, ["en-US"], this._ignore)) {
        errors.push("Name is Not Valid");
      }

      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      // next route
      next();
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
      });
    }
  };
}

module.exports = new PemasokValidator();
