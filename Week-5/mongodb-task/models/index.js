const { MongoClient } = require("mongodb");
// Connection URI
const uri = process.env.MONGO_URI;
// Create a new MongoClient
const connection = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

module.exports = class MongoInit {
  constructor() {
    this.init();
    this._ignore = { ignore: " -0123456789" };
  }
  init = async () => {
    try {
      await connection.connect();
      await connection.db('penjualan_afternoon').command({ping: 1})
      this.setDb();
      return;
    } catch (error) {
      console.log(error);
    }
  };

  setDb = () => {
    this._connection = connection.db("penjualan_afternoon");
    this._transaksi = this._connection.collection("transaksi");
    this._barang = this._connection.collection("barang");
    this._pemasok = this._connection.collection("pemasok");
    this._pelanggan = this._connection.collection("pelanggan");
  };
};
