const express = require("express");
const router = express.Router();
// import controller and validator
const PemasokController = require("../controllers/pemasokController.js");
const PemasokValidator = require("../middlewares/validators/pemasokValidator.js");
// set CRUD router
router.get("/", PemasokController.getAll);
router.get("/:id", PemasokValidator.getOne, PemasokController.getOne);
router.post("/", PemasokValidator.createPemasok, PemasokController.createPemasok);
router.put("/:id", PemasokValidator.updatePemasok, PemasokController.updatePemasok);
router.delete("/:id", PemasokValidator.getOne, PemasokController.deletePemasok);
// export router object
module.exports = router;
