// import module and setup router
const express = require('express');
const router = express.Router()
// import controller and validator
const transactionController = require('../controllers/transaksiController.js');
const transactionValidator = require('../middlewares/validators/transaksiValidator.js')
// set CRUD router
router.get('/', transactionController.getAll);
router.get('/:id', transactionValidator.getOne, transactionController.getOne);
router.post('/', transactionValidator.createTransaksi, transactionController.createTransaksi);
router.put('/:id', transactionValidator.updateTransaksi, transactionController.updateTransaksi);
router.delete('/:id', transactionValidator.getOne, transactionController.deleteTransaksi)
// export router object
module.exports = router;