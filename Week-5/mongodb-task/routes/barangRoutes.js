const express = require('express');
const validator = require('validator')

const barangController = require('../controllers/barangController.js');
const barangValidator = require('../middlewares/validators/barangValidator.js')
const router = express.Router()

router.get('/', barangController.getAll);
router.get('/:id', barangValidator.getOne, barangController.getOne);
router.post('/', barangValidator.createBarang, barangController.createBarang);
router.put('/:id', barangValidator.updateBarang, barangController.updateBarang);
router.delete('/:id', barangValidator.getOne, barangController.deleteBarang)

module.exports = router;