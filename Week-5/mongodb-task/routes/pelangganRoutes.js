const express = require("express");
const router = express.Router();
// import controller and validator
const PelangganController = require("../controllers/pelangganController.js");
const PelangganValidator = require("../middlewares/validators/pelangganValidator.js");
// set CRUD router
router.get("/", PelangganController.getAll);
router.get("/:id", PelangganValidator.getOne, PelangganController.getOne);
router.post("/", PelangganValidator.createPelanggan, PelangganController.createPelanggan);
router.put("/:id", PelangganValidator.updatePelanggan, PelangganController.updatePelanggan);
router.delete("/:id", PelangganValidator.getOne, PelangganController.deletePelanggan);
// export router object
module.exports = router;
