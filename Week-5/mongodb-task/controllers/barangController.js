const { ObjectId } = require("mongodb"); // Import ObjectId
const MongoInit = require("../models");

// controller barang class extend connection from monggoinit class
class BarangController extends MongoInit {
  constructor() {
    super();
  }
  // controller method for all item request
  getAll = async (req, res) => {
    try {
      // get all barang data
      const data = await this._barang.find({}).toArray();
      // if there is no data in the collection
      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for single item request
  getOne = async (req, res) => {
    try {
      // get one data
      const data = await this._barang.findOne({
        _id: ObjectId(req.params.id),
      });

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for create item request
  createBarang = async (req, res) => {
    try {
      // create one data
      const data = await this._barang.insertOne({
        nama: req.body.nama_barang,
        harga: req.body.harga_barang,
        pemasok: req.body.pemasok,
      });
      return res.status(200).json({
        message: "Success",
        data: data.ops[0],
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for update item request
  updateBarang = async (req, res) => {
    try {
      await this._barang.updateOne(
        {
          _id: ObjectId(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama_barang,
            harga: parseInt(req.body.harga_barang),
            pemasok: req.body.pemasok,
          },
        }
      );
      // return the updated data
      const updated = await this._barang.findOne({
        _id: ObjectId(req.params.id),
      });

      res.status(200).json({
        message: "Success",
        data: updated,
      });
    } catch (e) {}
  };
  // controller method for delete item request
  deleteBarang = async (req, res) => {
    try {
      // detail deleted item
      const deleted = await this._barang.findOne({
        _id: ObjectId(req.params.id),
      });
      await this._barang.deleteOne({
        _id: ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success to delete barang",
        data: deleted,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
}

module.exports = new BarangController();
