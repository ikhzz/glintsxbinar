const { ObjectId } = require("mongodb"); // Import ObjectId
const MongoInit = require("../models");

// controller pemasok class extend connection from monggoinit class
class PelangganController extends MongoInit {
  constructor() {
    super();
  }
  // controller method for all item request
  getAll = async (req, res) => {
    try {
      // get all pelanggan data
      const data = await this._pelanggan.find({}).toArray();
      // if there is no data in the collection
      if (data == null) {
        res.status(400).json({
          message: "No Data",
        });
      }

      res.status(200).json({
        message: "Success",
        data: data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for single item request
  getOne = async (req, res) => {
    try {
      // get one data
      const data = await this._pelanggan.findOne({
        _id: ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for create item request
  createPelanggan = async (req, res) => {
    try {
      const data = await this._pelanggan.insertOne({
        nama: req.body.nama_pelanggan,
      });
      return res.status(200).json({
        message: "Success",
        data: data.ops[0],
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for update item request
  updatePelanggan = async (req, res) => {
    try {
      await this._pelanggan.updateOne(
        {
          _id: ObjectId(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama_pelanggan,
          },
        }
      );
      const updated = await this._pelanggan.findOne({
        _id: ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success",
        data: updated,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for delete item request
  deletePelanggan = async (req, res) => {
    try {
      // detail deleted item
      const deleted = await this._pelanggan.findOne({
        _id: ObjectId(req.params.id),
      });
      await this._pelanggan.deleteOne({
        _id: ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success to delete pemasok",
        "Deleted Item": deleted,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
}

module.exports = new PelangganController();
