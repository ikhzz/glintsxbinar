const { ObjectId } = require("mongodb"); // Import ObjectId
const MongoInit = require("../models");

// controller transaksi class extend connection from monggoinit class
class TransaksiController extends MongoInit {
  constructor() {
    super();
  }
  // controller method for all item request
  getAll = async (req, res) => {
    try {
      // get all data
      let data = await this._transaksi.find({}).toArray(); // Get all data from transaksi table

      // If no data
      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };

  // controller method for single item request
  getOne = async (req, res) => {
    try {
      // Find one data
      let data = await this._transaksi.findOne({
        _id: new ObjectId(req.params.id),
      });

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };

  // controller method for create item request
  createTransaksi = async (req, res) => {
    try {
      // Insert data transaksi
      let data = await this._transaksi.insertOne({
        barang: req.body.barang,
        pelanggan: req.body.pelanggan,
        jumlah: parseInt(req.body.jumlah),
        total: req.body.total,
      });

      // If success
      return res.status(200).json({
        message: "Success",
        data: data.ops[0],
      });
    } catch (e) {
      console.log(e);
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };

  // controller method for update item request
  updateTransaksi = async (req, res) => {
    try {
      // Update data transaksi
      await this._transaksi.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            barang: req.body.barang,
            pelanggan: req.body.pelanggan,
            jumlah: parseInt(req.body.jumlah),
            total: req.body.total,
          },
        }
      );

      // Find data that updated
      let data = await this._transaksi.findOne({
        _id: new ObjectId(req.params.id),
      });

      // If success

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };

  // controller method for delete item request
  deleteTransaksi = async (req, res) => {
    try {
      // detail deleted item
      let data = await this._transaksi.findOne({
        _id: new ObjectId(req.params.id),
      });
      // delete data depends on req.params.id
      await this._transaksi.deleteOne({
        _id: new ObjectId(req.params.id),
      });

      // If success
      return res.status(200).json({
        message: "Success to delete transaksi",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
}

module.exports = new TransaksiController();
