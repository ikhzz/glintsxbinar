const { ObjectId } = require("mongodb"); // Import ObjectId
const MongoInit = require("../models");

// controller pemasok class extend connection from monggoinit class
class PemasokController extends MongoInit {
  constructor() {
    super();
  }
  // controller method for all item request
  getAll = async (req, res) => {
    try {
      // get all pemasok data
      const data = await this._pemasok.find({}).toArray();
      // if there is no data in the collection
      if (data == null) {
        res.status(400).json({
          message: "No Data",
        });
      }

      res.status(200).json({
        message: "Success",
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for single item request
  getOne = async (req, res) => {
    try {
      // get one data
      const data = await this._pemasok.findOne({
        _id: ObjectId(req.params.id),
      });
      res.status(200).json({
        message: "Success",
        data,
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for create item request
  createPemasok = async (req, res) => {
    try {
      const data = await this._pemasok.insertOne({
        nama: req.body.nama_pemasok,
      });
      return res.status(200).json({
        message: "Success",
        data: data.ops[0],
      });
    } catch (error) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for update item request
  updatePemasok = async (req, res) => {
    try {
      await this._pemasok.updateOne(
        {
          _id: ObjectId(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama_pemasok,
          },
        }
      );

      const updated = await this._pemasok.findOne({
        _id: ObjectId(req.params.id),
      });

      return res.status(200).json({
        message: "Success",
        data: updated,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
  // controller method for delete item request
  deletePemasok = async (req, res) => {
    try {
      // detail deleted item
      const deleted = await this._pemasok.findOne({
        _id: ObjectId(req.params.id),
      });
      await this._pemasok.deleteOne({
        _id: ObjectId(req.params.id),
      });
      return res.status(200).json({
        message: "Success to delete pemasok",
        deleted: deleted,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  };
}

module.exports = new PemasokController();
