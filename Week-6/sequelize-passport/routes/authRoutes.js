const express = require("express"); // Import express
const router = express.Router(); // Make router from app
// import controller, validator, and auth middleware
const AuthController = require("../controllers/authController.js"); // Import TransaksiController
const AuthValidator = require("../middlewares/validators/authValidator");
const PassportMiddleware = require("../middlewares/auth"); // Import validator to validate every request from user
// setup route
router.post("/signup", AuthValidator.signup, PassportMiddleware.signup, AuthController.getToken);

router.post("/signin", AuthValidator.signin,PassportMiddleware.signin, AuthController.getToken);

module.exports = router; // Export router