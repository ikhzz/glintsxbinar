const express = require("express"); // Import express
const passport = require("passport");
const AuthController = require("../controllers/authController.js"); // Import TransaksiController
const PassportCustom = require("../middlewares/auth"); // Import validator to validate every request from user
const router = express.Router(); // Make router from app
const AuthValidator = require("../middlewares/validators/authValidator")

router.post("/signup", AuthValidator.signup, PassportCustom.signUpRoutes, AuthController.getToken);

router.post("/signin", AuthValidator.signin, PassportCustom.signInRoutes, AuthController.getToken);

module.exports = router; // Export router