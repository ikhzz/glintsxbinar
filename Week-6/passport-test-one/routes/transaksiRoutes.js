const express = require('express') // Import express
const router = express.Router() // Make router from app
const TransaksiController = require('../controllers/transaksiController.js') // Import TransaksiController
const TransaksiValidator = require('../middlewares/validators/transaksiValidator.js') // Import validator to validate every request from user
const PassportCustom = require('../middlewares/auth')


router.get('/', PassportCustom.userCheck,TransaksiController.getAll) // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get('/:id', PassportCustom.userCheck,TransaksiValidator.getOne, TransaksiController.getOne) // If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.post('/', PassportCustom.adminCheck, TransaksiValidator.createTransaksi, TransaksiController.create) // If accessing localhost:3000/transaksi/create, it will call create function in TransaksiController class
router.put('/:id', PassportCustom.adminCheck, TransaksiValidator.updateTransaksi, TransaksiController.updateTransaksi) // If accessing localhost:3000/transaksi/update/:id, it will call update function in TransaksiController class
router.delete('/:id', PassportCustom.adminCheck, TransaksiValidator.deleteTransaksi, TransaksiController.deleteTransaksi) // If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class

module.exports = router; // Export router
