const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}

// Should return array
function sortAscending(data) {
  // Code Here
  data = clean(data)
  let go = true

  while(go){
    for(let i = 0; i < data.length; i++){
      if(data[i] > data[i + 1]){
        // let temp = data[i]
        // data[i] = data[i+1]
        // data[i+1] = temp
        // reference from facilitator/ mr fahmi alfareza 
        [data[i], data[i+1]] = [data[i+1], data[i]]
        break;
      } else if(data[i] == data[i + 1] || data[i] < data[i + 1]) {
        continue
      } else {
        go =false
      }
    }
  }
  return data;
}

// Should return array
function sortDecending(data) {
  // Code Here
  data = clean(data)
  let go = true

  while(go){
    for(let i = 0; i < data.length; i++){
      if(data[i] < data[i + 1]){
        // let temp = data[i]
        // data[i] = data[i+1]
        // data[i+1] = temp
        // reference from facilitator/ mr fahmi alfareza 
        [data[i], data[i+1]] = [data[i+1], data[i]]
        break;
      } else if(data[i] == data[i + 1] || data[i] > data[i + 1]) {
        continue
      } else {
        go =false
      }
    }
  }
  return data;
}
sortAscending(data)
sortDecending(data)
// DON'T CHANGE
test(sortAscending, sortDecending, data);