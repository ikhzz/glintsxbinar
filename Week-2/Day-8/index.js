const start = () => {
  console.log("Buka Web Academy Glints")
  glintTest()
}

const glintTest = () => {
  console.log("process test Bahasa inggris, logika dan research")
  ifPassed(true)
}

const ifPassed = (result) => {
  if(result == true) {
    interviewTest()
  } else {
    console.log("Maaf Anda tidak lulus")
  }
}

const interviewTest = () => {
  console.log("Tes Wawancara")
  inputDocument()
}

const inputDocument = () => {
  console.log("Input Dokumen dan Pembayaran")
  chooseStack("front-end")
}

const chooseStack = (choice) => {
  passed(choice)
}


const passed = (stack) => {
  console.log(`Selamat Anda Lulus di ${stack}`)
}

start();