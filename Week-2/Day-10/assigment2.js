const data = [
  {
    name: "John",
    status: "positive"
  },
  {
    name: 'Mike',
    status: "suspect"
  },
  {
    name: '????',
    status: 'positive'
  }
]

let param = 'positive';

switch (param) {
  case 'positive':
    const positive = []
    data.forEach( e => {
      if(e.status == 'positive'){
        positive.push(e.name)
      }
    })
    console.log(positive)
    break;
  case 'suspect':
    const suspect = []
    data.forEach( e => {
      if(e.status == 'suspect'){
        suspect.push(e.name)
      }
    })
    console.log(suspect)
    break;
  default:
    console.log('No more patient')
}