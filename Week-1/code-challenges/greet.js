// Import readline library to take input from user
const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
})

const greet = (name, address, birthday) => {
  name = name.replace(name[0], name[0].toUpperCase())
  address = address.replace(address[0], address[0].toUpperCase())
  birthday = new Date().getFullYear() - birthday;
  // print the message based on input
  console.log(`Hello, ${name}, Looks like you're ${birthday}! And you lived in ${address}! `)
}

// DON'T CHANGE
console.log("Goverment Registry\n");
// GET User's Name
rl.question("What is your name? ", (name) => {
  // GET User's Address
  rl.question("Which city do you live? ", (address) => {
    // GET User's Birthday
    rl.question("When was your birthday year? ", (birthday) => {
      greet(name, address, birthday);

      rl.close();
    });
  });
});

rl.on("close", () => {
  process.exit();
});
