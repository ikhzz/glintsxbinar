// Import readline library to take input from user
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
})

const prismVolume = () => {
  // ask for value of base area
  readline.question('For prism volume i need base area ? ', basearea => {
    // ask for value of height
    readline.question('I Also need height ? ', height => {
      // print the volume of prism
      if(!isNaN(basearea) && !isNaN(height)){
        console.log(`Your Prism Volume is: ${basearea * height}`);
        tubeVolume()
      } else {
        console.log('Wrong input')
        prismVolume()
      }
    })
  }) 
}

const tubeVolume = () => {
  // declare pi from built-in function
  const pi = Math.PI;
  // ask for value of circle radius
  readline.question('For tube volume i need circle radius ? ', circleRadius => {
    // ask for value of height
    readline.question('I Also need height ? ', circleHeight => {
      if(!isNaN(circleHeight) && !isNaN(circleRadius)){
        // print the volume of tube
        console.log(`Your Tube Volume is ${(pi * (circleRadius * 2) * circleHeight).toFixed(2)}`);
        readline.close()
      } else {
        console.log('Wrong input')
        tubeVolume()
      }    
    })
  })
}
prismVolume()