# GlintsXBinar Repo For Code Challenge by Mohammad Zul Fikri Dwi Ananta

## Code Challenge for week 1

### Gif for geometry.gif/preview task 1
<img src="./Week-1/code-challenges/geometry.gif" width="400" height="400">

### Gif for greet.gif/preview task 2
<img src="./Week-1/code-challenges/greet.gif" width="400" height="400">

## Daily Task day 7 for week 2

### No preview, Build 4 volume formula and call it in index.js  

## Daily Task day 8 for week 2

### Png for function diagram
<img src='./Week-2/Day-8/diagram tugas.png' width="300" height="300">

## Daily Task day 10 for week 2

### Assigment-1 sort array and remove one value

### Assigment-2 switch case and for loop data array of object

### Assigment-3 Group Task create a function to filter the null in array

## Code Challenge for week 2

### No preview, bubble sort array of number asc, desc function

## Daily Task day 11 for week 3

### Assigment-1 require file after login success

## Daily Task day 13 for week 3

### Assigment-1 create 4 geometry child class of 3 Dimentional Class from Geometry Class 
<img src='./Week-3/Day-13/day-13.gif' width="400" height="400">

## Daily Task day 14 for week 3

### Assigment create async/await and promise function to read 10 text files
<img src='./Week-3/Day-14/Day-14.gif' width="400" height="400">

## Code Challenge for week 3

### Create url with our name and send our full name as response
<img src='./Week-3/code-challenge/code-challenge.gif' width="400" height="400">